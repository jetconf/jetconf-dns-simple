# jetconf-dns-simple

DNS server managed via JetConf with a simplified but platform-neutral data model

* [DEMO examples](https://gitlab.labs.nic.cz/jetconf/jetconf-dns-simple/wikis/demo/)
* [Installation](https://gitlab.labs.nic.cz/jetconf/jetconf-dns-simple/wikis/installation)
* [ASCII tree](https://gitlab.labs.nic.cz/jetconf/jetconf-dns-simple/raw/master/yang-modules/model.tree)
* [YANG module `cz-nic-dns-slave-server`](https://gitlab.labs.nic.cz/jetconf/jetconf-dns-simple/raw/master/yang-modules/cznic-dns-slave-server.yang)

## Web-based data browser

* [Jetscreen](https://jetconf.pages.labs.nic.cz/jetscreen/)

Currently this web app can be used mainly for viewing the data,
editing functions mostly don't work yet.

## Links

* [JetConf](https://gitlab.labs.nic.cz/labs/jetconf)
* [YANG](https://tools.ietf.org/html/rfc6020)
* [KnotDNS](https://www.knot-dns.cz/)
* [PowerDNS](https://www.powerdns.com/) 

