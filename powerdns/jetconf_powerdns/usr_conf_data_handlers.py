from typing import List, Dict, Union, Any

from yangson.instance import InstanceRoute, ObjectValue, EntryKeys

from jetconf import config
from jetconf.data import BaseDatastore, ChangeType, DataChange
from jetconf.helpers import ErrorHelpers, LogHelpers
from jetconf.handler_base import ConfDataObjectHandler, ConfDataListHandler

from . import shared_objs as so

JsonNodeT = Union[Dict[str, Any], List]
epretty = ErrorHelpers.epretty
debug_confh = LogHelpers.create_module_dbg_logger(__name__)


# ---------- User-defined handlers follow ----------

class RootHandler(ConfDataObjectHandler):
    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

        root_nv = self.ds.get_data_root().add_defaults().value

        so.PDNS.generate_bind_file(
            config=root_nv
        )

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

        root_nv = self.ds.get_data_root().add_defaults().value

        so.PDNS.generate_bind_file(
                     config=root_nv
                )

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")

        root_nv = self.ds.get_data_root().add_defaults().value

        so.PDNS.generate_bind_file(
            config=root_nv
        )


def commit_begin():
    debug_confh("Connecing to PowerDNS http API")


def commit_end(failed: bool=False):

    if failed:
        debug_confh("Aborting PowerDNS transaction")
    else:
        debug_confh("Commiting PowerDNS transaction")


def register_conf_handlers(ds: BaseDatastore):
    ds.handlers.conf.register(RootHandler(ds, "/"))

    # Set datastore commit callbacks
    ds.handlers.commit_begin = commit_begin
    ds.handlers.commit_end = commit_end
