from colorlog import error, info

from jetconf import config
from . import shared_objs as so


def jc_startup():
    info("Backend: init")


def jc_end():
    info("Backend: cleaning up")
    so.PDNS = None
