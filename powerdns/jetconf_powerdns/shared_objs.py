from .pdns_client import PdnsClient

PDNS = None                 # type: PdnsClient
ZONES_DIR = "/etc/powerdns"  # type: str
