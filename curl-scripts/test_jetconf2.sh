#!/bin/bash

# full test
sh post_new_remote_server.sh remote-server-jetconf.json
echo ""
sh post_new_remote_server.sh remote-server-ns-update.json
echo ""
sh post_new_zone.sh zone-master-example.json
echo ""
sh post_new_zone.sh zone-slave-turris.json
echo ""
sh post_allow_update.sh example. ns-update
echo ""
sh restart_server.sh
echo ""

