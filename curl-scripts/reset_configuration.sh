#!/bin/bash

source ./http_conf

PUT_RS='{"cznic-dns-server-simple:remote-server": []}'
PUT_Z='{"cznic-dns-server-simple:zone": []}'


echo "--- PUT empty configuration to remote-server"
URL="${JC_URL}/restconf/data/cznic-dns-server-simple:dns-server/remote-server"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X PUT -d "$PUT_RS" "$URL"

echo "--- PUT empty configuration to zone"
URL="${JC_URL}/restconf/data/cznic-dns-server-simple:dns-server/zones/zone"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X PUT -d "$PUT_Z" "$URL"

echo "--- POST conf-commit"
URL="${JC_URL}/restconf/operations/jetconf:conf-commit"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"
