#!/bin/bash

source ./http_conf

if [ -z "$1" ]
then
	echo "Usage: $0 <zone_id>"

else
	echo "--- DELETE allow-update from configuration"
	URL="${JC_URL}/restconf/data/cznic-dns-server-simple:dns-server/zones/zone=$1/allow-update"
	curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X DELETE "$URL"

	echo "--- POST conf-commit"
	URL="${JC_URL}/restconf/operations/jetconf:conf-commit"
	curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"
fi


