#!/bin/bash

source ./http_conf

if [ -z "$2" ]
then
	echo "Usage: $0 <zone_id> <remote_name>"

else
	POST_DATA="{\"cznic-dns-server-simple:allow-update\": \"${2}\"}"

	echo "--- POST allow-update to configuration"
	URL="${JC_URL}/restconf/data/cznic-dns-server-simple:dns-server/zones/zone=$1"
	curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST -d "$POST_DATA" "$URL"

	echo "--- POST conf-commit"
	URL="${JC_URL}/restconf/operations/jetconf:conf-commit"
	curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"
fi


