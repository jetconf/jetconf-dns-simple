#!/bin/bash

source ./http_conf

# rpc operation jetconf:conf-reset

echo "--- POST Operation 'jetconf:conf-reset'"
URL="${JC_URL}/restconf/operations/jetconf:conf-reset"
	curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"
