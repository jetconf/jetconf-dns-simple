#!/bin/bash

# full test
sh reset_configuration.sh
echo ""
sh post_new_remote_server.sh remote-server-jetconf2.json
echo ""
sh post_new_remote_server.sh remote-server-ns-update.json
echo ""
sh post_new_zone.sh zone-master-turris.json
echo ""
sh post_new_zone.sh zone-slave-example.json
echo ""
sh post_allow_update.sh turris.cz. ns-update
echo ""
sh restart_server.sh
echo ""

