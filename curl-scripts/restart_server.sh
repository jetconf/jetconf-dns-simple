#!/bin/bash

source ./http_conf

# rpc operation restart server

URL="${JC_URL}/restconf/operations/cznic-dns-server-simple:restart-server"
POST_DATA='{"cznic-dns-slave-server:input": {}}'

echo "--- POST Operation 'restart-server'"

curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST -d "$POST_DATA" "$URL"
