#!/bin/bash

source ./http_conf

echo "--- GET /restconf/operations"
URL="${JC_URL}/restconf/operations"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X GET "$URL"

