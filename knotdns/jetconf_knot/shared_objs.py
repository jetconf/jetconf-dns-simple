from .knot_api import KnotConfig

KNOT = None     # type: KnotConfig
ZONES_DIR = "${localstatedir}/lib/knot/zones"  # type: str
